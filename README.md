1. Be sure NPM is installed (included with Node.js at https://nodejs.org/en/download/)
2. Using command line navigate to root folder
3. Run 'npm install', wait for dependencies to be downloaded
4. Run 'npm watch', a file watcher should now be running and responding to changes made in "./src"
5. To build a fully minified release version of the Javascript run 'npm release'
6. Launch './dist/index.html' in a browser
