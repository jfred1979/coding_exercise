/**
 * Created by Jake on 4/29/2017.
 */
import { TweenLite } from 'gsap';

export class Main {
    constructor() {
        this.tracks = [];
        this.tracksLoaded = 0;
        this.tracksToLoadNum = -1;
        this.currentTrack = null;

        this.loader = document.createElement('img');
        this.loader.onload = function() {
            this.showSetControls();
            document.body.appendChild(this.setContainer);
        }.bind(this);
        this.loader.id = 'loader';

        this.setContainer = document.createElement('div');
        this.setContainer.className = 'setContainer';

       this.setControls = new SetControls('https://soundcloud.com/noisia/sets/outer-edges-remixes', function(setURL) {
           this.showLoader();
           this.clearSet();
           this.loadSet(setURL);
       }.bind(this));

        var SoundCloudAudio = require('soundcloud-audio');
        this.scPlayer = new SoundCloudAudio('15b17a847df647ea42a4aea862896f6c');

        this.loader.src = './assets/loader.gif';
    }

    showLoader() {
        document.body.appendChild(this.loader);
    }

    hideLoader() {
        document.body.removeChild(this.loader);
    }

    showSetControls() {
        this.setControls.getDisplay().style.opacity = '0';
        document.body.appendChild(this.setControls.getDisplay());
        TweenLite.to(this.setControls.getDisplay(), 1, {css:{opacity: '1'}});
    }

    loadSet(url) {
        this.scPlayer.resolve(url, function (playlist) {
            //console.log(playlist);
            this.tracksToLoadNum = playlist.tracks.length;
            for(var i = 0; i < this.tracksToLoadNum; i++) {
                var track = new TrackView(playlist.tracks[i], i, this.trackViewActionHandler.bind(this), this.trackViewLoadedHandler.bind(this));
                track.getDisplay().style.opacity = '0';
                this.tracks[i] = track;
            }
            this.scPlayer.on('ended', function () {
                this.currentTrack.setState('stopped');
            }.bind(this));
            if(this.tracksToLoadNum == 0){
                alert('No tracks found in this set!');
                this.trackViewLoadedHandler();
            }
        }.bind(this));
    }

    clearSet() {
        this.tracksLoaded = 0;
        this.tracks = [];
        this.currentTrack = null;
        this.setContainer.innerHTML = '';
    }

    trackViewLoadedHandler() {
        this.tracksLoaded++;
        if(this.tracksLoaded >= this.tracksToLoadNum) {
            this.hideLoader();
            for(var i = 0; i < this.tracks.length; i++) {
                var track = this.tracks[i];
                this.setContainer.appendChild(track.getDisplay());
                TweenLite.to(track.getDisplay(), 0.5, {css:{opacity: '1'}, delay: 0.1 * i})
            }
        }
    }

    trackViewActionHandler(trackView, action) {
        switch(action) {
            case 'playPressed':
                this.scPlayer.stop();
                for(var i = 0; i < this.tracks.length; i++) {
                    var track = this.tracks[i];
                    if(track != trackView) {
                        track.setState('stopped');
                    } else {
                        track.setState('playing');
                    }
                }
                this.scPlayer.play({ playlistIndex: trackView.getIndex() });
                this.currentTrack = trackView;
                break;

            case 'stopPressed':
                trackView.setState('stopped');
                this.scPlayer.stop();
                this.currentTrack = null;
                break;
        }
    }
}

class SetControls {
    constructor(defaultURL, setChangedCallback) {
        this.container = document.createElement('div');
        this.container.className = 'setControlsContainer';
        this.header = document.createElement('h1');
        this.header.appendChild(document.createTextNode('Enter a Soundcloud Set (Playlist) URL'));
        this.inputContainer = document.createElement('div');
        this.setURLInput = document.createElement('input');
        this.setURLInput.placeholder = 'Soundcloud set URL';
        this.setURLInput.value = defaultURL;
        this.setURLButton = document.createElement('button');
        this.setURLButton.type = 'button';
        this.setURLButton.appendChild(document.createTextNode('Load'));
        this.setURLButton.onclick = function() {
            setChangedCallback(this.setURLInput.value);
        }.bind(this);
        this.container.appendChild(this.header);
        this.inputContainer.appendChild(this.setURLInput);
        this.inputContainer.appendChild(this.setURLButton);
        this.container.appendChild(this.inputContainer);
    }

    getDisplay() {
        return this.container;
    }
}

class TrackView {
    constructor(trackData, index, actionCallback, loadedCallback) {
        this.currentState = null;
        this.actionCallback = actionCallback;
        this.trackData = trackData;
        this.index = index;
        trackData.title = this.shortenString(trackData.title, 16);
        trackData.description = this.shortenString(trackData.description, 128);

        this.container = document.createElement('div');
        this.container.className = 'trackContainer';

        this.header = document.createElement('a');
        this.header.href = trackData.permalink_url;
        this.header.appendChild(document.createTextNode(trackData.title));
        this.header.className = 'trackHeader';

        this.description = document.createElement('p');
        this.description.appendChild(document.createTextNode(trackData.description));
        this.description.className = 'trackDescription';

        this.trackDisplayContainer = document.createElement('div');
        this.trackDisplayContainer.className = 'trackDisplay';
        this.trackDisplayContainer.onclick = function() {
            this.actionCallback(this, this.currentState == 'playing' ? 'stopPressed' : 'playPressed');
        }.bind(this);
        this.trackDisplayImage = document.createElement('img');
        this.trackDisplayImage.onload = function() {
            loadedCallback();
        }.bind(this);
        this.trackDisplayImage.src = trackData.artwork_url;

        this.trackDisplayControls = document.createElement('div');
        this.trackDisplayControls.className = 'trackDisplayControls';
        this.trackDisplayPlayButton = document.createElement('img');
        this.trackDisplayPlayButton.src = 'assets/icon-play.png';
        this.trackDisplayStopButton = document.createElement('img');
        this.trackDisplayStopButton.src = 'assets/icon-stop.png';

        this.trackDisplayContainer.appendChild(this.trackDisplayControls);
        this.trackDisplayContainer.appendChild(this.trackDisplayImage);

        this.container.appendChild(this.header);
        this.container.appendChild(this.trackDisplayContainer);
        this.container.appendChild(this.description);

        this.setState('stopped');
    }

    setState(newState) {
        switch (this.currentState) {
            case "stopped":
                this.trackDisplayControls.removeChild(this.trackDisplayPlayButton);
                break;

            case "playing":
                this.trackDisplayControls.removeChild(this.trackDisplayStopButton);
                break;
        }
        switch(newState) {
            case "stopped":
                this.trackDisplayControls.appendChild(this.trackDisplayPlayButton);
                break;

            case "playing":
                this.trackDisplayControls.appendChild(this.trackDisplayStopButton);
                break;
        }
        this.currentState = newState;
    }

    getDisplay() {
        return this.container;
    }

    getTrackData() {
        return this.trackData;
    }

    getIndex() {
        return this.index;
    }

    shortenString(str, maxLength) {
        if(str.length > maxLength) {
            return str.slice(0, maxLength) + '...'
        } else {
            return str;
        }
    }
}

window.onload = function () {
    var main = new Main();
}
